const enviroment = require('../config/enviroment');
const fecth = require('node-fetch');

function OpenWeatherClient() { }

OpenWeatherClient.prototype.cityWeather = async function(nomeCidade) {
    return await fecth(`${enviroment.URL_WEATHER_API}/data/2.5/weather?q=${nomeCidade}&appId=${enviroment.ID_WEATHER_API}`)
        .then(res => {
            if(!res.ok)
                return Promise.reject(res.status);
            else
                return res.json();
        }).catch(err => {
            console.log(`Erro ao solicitar api weather Status: ${err}`);
        })
}

module.exports = function(){
    return OpenWeatherClient;
}