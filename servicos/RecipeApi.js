const enviroment = require('../config/enviroment');
const fecth = require('node-fetch');

function EdamanClient() { }

EdamanClient.prototype.getRecipe = async function(dishType) {
    return await fecth(`${enviroment.PATH_RECIPE_API}/search?q=${dishType}&app_id=${enviroment.ID_RECIPE_API}&app_key=${enviroment.KEY_RECIPE_API}`)
        .then(res => {
            if(!res.ok)
                return Promise.reject(res.status);
            else
                return res.json();
        }).catch(err => {
            console.log(`Erro ao solicitar api edaman Status: ${err}`);
        })
}

module.exports = function(){
    return EdamanClient;
}