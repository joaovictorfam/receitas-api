require('dotenv/config');
module.exports = {
    ENV: process.env.NODE_ENV || 'development',
    PORT: process.env.PORT || 5000,
    URL: process.env.BASE_URL || 'http://localhost:5000',
    ID_WEATHER_API: process.env.ID_WEATHER_API || '45dd39c3ad65d30f62e0d51c5a610cb0',
    URL_WEATHER_API: process.env.URL_WEATHER_API || 'https://api.openweathermap.org',
    PATH_RECIPE_API: process.env.PATH_RECIPE_API || 'https://api.edamam.com',
    ID_RECIPE_API: process.env.ID_RECIPE_API || '855dc3b2',
    KEY_RECIPE_API: process.env.KEY_RECIPE_API || 'd4511f8f146712f5ad3f36d956772b96',

}