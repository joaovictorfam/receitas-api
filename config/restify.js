const restify = require('restify');
const rateLimit = require("express-rate-limit");
const consign = require('consign');

module.exports = function() {
    var app = restify.createServer({
        name: 'api-de-receitas',
        version: '1.0.0'
    });

    app.use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header(
            "Access-Control-Allow-Headers",
            "Origin, X-Requested-With, Content-Type, Accept, Authorization"
        );
        if (req.method === 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'GET');
            console.log("Acess-Conrol-Allow-Methods sent on res.header");
            return res.status(200).json({});
        }
        next();
    });

    app.use(
        rateLimit({
            windowMs: 60 * 1000,
            max: 4,
            statusCode: 429,
            message: "my initial message",
            handler: function(req, res , next) {
                res.status(429);
                res.json("Too many requests. Status: 429");
            }
        })
    );

    consign()
        .include('routes')
        .then('servicos')
        .into(app);

    return app;
}

