const enviroment = require('../config/enviroment');

module.exports = function (app) {
    app.get('/cidades/:nome', async function (req, res, next) {
        var nomeCidade = req.params.nome;
        var regex = /^[0-9]+$/;
        if(regex.test(nomeCidade) || !nomeCidade){
            console.log('GET /cidades/:nome O nome da cidade e obrigatorio e nao pode conter numeros');
            res.status(400);
            res.json("Campo nome cidade incorreto ou inexistente");
            return next();
        }
        
        var openWeather = new app.servicos.openWeatherApi();
        var temperaturaCidade = await openWeather.cityWeather(nomeCidade);

        var dishType;
        if(temperaturaCidade){
            if(temperaturaCidade.main.temp < 293.15) {
                dishType = "soup"
            } else if(temperaturaCidade.main.temp < 302.15) {
                dishType = "meat"
            } else {
                dishType = "salad"
            }
            var edaman = new app.servicos.RecipeApi();
            var receitas = await edaman.getRecipe(dishType);  
            if(receitas)             
                res.json(receitas);
            else {
                res.status(404);
                res.json("Resource not found on edaman service");
            }
        } else {
            res.status(404);
            res.json("cidade nao encontrada");
        }
        next();
    });

}