** Simples Micro-serviço RESTful, feito em Node,js**

micro-serviço que aceita requisições RESTFul que tenham por parâmetro o
nome de uma cidade e retorna uma receita baseada na temperatura atual.

*Esta aplicação usa a API OpenWeatherMaps (https://openweathermap.org/) para obter
a temperatura atual baseada na cidade e a API edamam (https://developer.edamam.com/)
para obter as receitas.*

---

## Download & build local

Primeiro certifique-se de você possuí a versão mais recente do Node Instalado em sua máquina.
*(https://nodejs.org/)*

*Execute os comandos a seguir para subir o servidor localmente*

1. git clone https://joaovictorfam@bitbucket.org/joaovictorfam/receitas-api.git
2. cd receitas-api
3. npm install
4. npm start

---

O servidor deverá estar escutando na porta 5000 do localhost

---

## Arquivo de configuração .env

O projeto aceita que variáveis como a chave de acesso para outras apis sejam configuradas através de um arquivo .env
As variaveis são controladas pelo arquivo enviroment.js. Nele é possível encontrar o modelo do corpo das chaves necessárias.
(Para facilitar já deixei as chaves configuradas no proprio arquivo enviroment.js então não é necessário um arquivo .env para rodar o projeto.
